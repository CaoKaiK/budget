'''set of utility functions'''

import json

def read_config(config):
    '''input list of tuples of configuration for sqlite table'''
    names, dtypes , name_types = [], [], []

    for name, dtype in config:
        names.append(name)
        dtypes.append(dtype)
        name_types.append(f'{name} {dtype}')

    config_string = ', '.join(name_types)
    return config_string

def read_config_json(path):
    '''uses read_config to read configuration from file'''
    with open(path) as config_file:
        config = json.load(config_file)

    config_string = read_config(config)

    return config_string


