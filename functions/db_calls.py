import logging
import json
import os

import sqlite3

logger = logging.getLogger('__main__')

PATH = os.path.abspath('database')

# load table configs
with open(os.path.join(PATH,"transactions.json")) as json_file:
    TRANSACTIONS_CONFIG = json.load(json_file)


def create_transaction(conn, transaction):
    # variables defined in config
    vars = [item[0] for item in TRANSACTIONS_CONFIG]
    vars_string = ','.join(map(str, vars))
    vals = ['?']*len(vars)
    vals_string = ','.join(map(str, vals))
    sql_command = (f'''INSERT INTO transactions({vars_string})'''
                    f''' VALUES({vals_string})''')
    cur = conn.cursor()
    cur.execute(sql_command, transaction)
    conn.commit()
    logger.debug(f'Written transaction row {cur.lastrowid}')
    return None

def delete_transaction(conn, rowid):
    sql_command = ('DELETE FROM transactions where rowid=?')
    cur = conn.cursor()
    cur.execute(sql_command, (rowid,))
    conn.commit()
    logger.debug(f'Deleted row {rowid} - Current rows {cur.lastrowid}')
    return None

