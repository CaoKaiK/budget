'''initialize databank'''

import logging
import os
import json
import sqlite3

from .utils import read_config

logger = logging.getLogger('__main__')

# define database path
PATH = os.path.abspath('database')
DB_NAME = 'accounts.db'


# define transactions configurations
TRANSACTIONS_NAME = 'transactions'
TRANSACTIONS_COLUMNS = [('account_id', 'int'),
                        ('date', 'text'),
                        ('amount', 'real'),
                        ('IBAN', 'text'),
                        ('BIC', 'text'),
                        ('recipient', 'text'),
                        ('message', 'text'),
                        ('main_cat', 'text'),
                        ('sub_cat', 'text'),
                        ('comment', 'text')]

config_string = read_config(TRANSACTIONS_COLUMNS)

TRANSACTIONS_CONFIG = f'{TRANSACTIONS_NAME} ({config_string})'

# define property configurations
PROPERTY_NAME = 'property'
PROPERTY_COLUMNS = [('property_id', 'int'),
                    ('city', 'text')]

config_string = read_config(PROPERTY_COLUMNS)

PROPERTY_CONFIG = f'{PROPERTY_NAME} ({config_string})'


def db_consistency(cur, conn, table_name, table_config, table_columns):
    '''check for existence and compare column names + types'''
    name = (table_name,)
    cur.execute('''SELECT name FROM sqlite_master WHERE type='table' AND name=?''', name)

    if not cur.fetchall():
        # if result is empty table does not exist -> create
        cur.execute(f'CREATE TABLE {table_config}')
        conn.commit()
        logger.debug(f"Created '{table_name}' table")
        # save configuration to json file
        with open(os.path.join(PATH,table_name+'.json'), 'w') as file:
            json.dump(table_columns, file)
        logger.debug(f"Saved '{table_name}' config")
    
    else:
        #TODO
        # PRAGMA table_info(transactions) check against config
        # if not consistent find method to create new table and merge data
        # delete old table
        logger.debug("Table 'transaction' already exists in defined config")
    return None

def init_db():
    '''create db if it does not exist and connect to db'''
    # sqlite version
    logger.debug(f'SQLite version - {sqlite3.version}')

    # creates database folder
    os.makedirs(PATH, exist_ok=True)

    # accounts
    logger.debug(f'Connecting - {DB_NAME}')
    conn = sqlite3.connect(os.path.join(PATH, DB_NAME))
    cur = conn.cursor()
    logger.debug(f'Connected - {DB_NAME}||{cur}')

    # check for transaction table
    db_consistency(cur, conn, TRANSACTIONS_NAME, TRANSACTIONS_CONFIG, TRANSACTIONS_COLUMNS)

    # check for transaction table
    db_consistency(cur, conn, PROPERTY_NAME, PROPERTY_CONFIG, PROPERTY_COLUMNS)

    return conn